$Id: README.txt, v 1.0 2011/06/01 02:20:30 louis Exp$

DESCRIPTION
--------------------------
The MResource module allows you to associate Organic Group members with Resources and Member Lists. A Resource is a third-party website that requires login information to view the website. A Member List is a list of user credentials mapped to Drupal users by email address (Resource and OG association also happens in a Member List node). By creating this association, you can have og members automatically sign in without knowing their credentials. Credentials specified on a Member List can be unique to each Drupal user or static, general logins for all group members.


REQUIRED MODULES
--------------------------
-og 
-views
-imagecache
-content
--link
--fieldgroup
--text
--imagefield
--nodereference
--optionwidgets


INSTALLATION
--------------------------
0. Make sure you have created a 'group' node.
1. Upload and activate the MResource module
2. Import content type "Resource" (in content_types/resource.txt)
3. Import view "ResourceList" (in views/resourceList-VIEW.txt)
4. Import content type "Member List" (in content_types/member_list.txt)
5. Import view "Resources" (in views/resources-VIEW.txt)


TROUBLESHOOTING
--------------------------
- Check aliases if pathauto is installed for new content types.
- When uploading CSV files, make sure to verify with a plain text editor. Some Macintosh programs create inconsistent line endings that will confuse PHP parsing. 
- CSV files should not have a trailing comma or delimiter.


NOTES
--------------------------
- There is a default ImageCache setting for the Resources view called resource_view_thumb. You can override this in the ImageCache settings.
- When a user is moved out of an Organic Group, the associated member_list nodes for that group are scanned for that user and removed as well. A backup of the original file is created in same directory.


TODO/BUGS/FEATURE REQUESTS
----------------
- Send requests to louis AT devnalysis DOT com
- If may be nice to identify the Resource, Member List, and OG node types in an admin setting.
- We might want to have the automatic removal/modification of CSV files (when member leaves a group) based on some global option per installation.
- It might be nice to describe the expected resource values when selecting the resource on the node reference field. Some ajaxy call that displays what that resource looks like and expects.
- Hard code content types and views for easier installation.



CREDITS
----------------------------
Authored and maintained by Louis Chavez <louis AT devnalysis DOT com>
Contributors: Dan Serrato, Mark Hammons
Sponsored by Fresno County Office of Education - http://www.fcoe.org
