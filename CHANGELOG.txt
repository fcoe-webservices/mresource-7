// $Id: CHANGELOG.txt,v 1.0.0.1 2011/09/29 08:00:00 louis Exp $

Changes since DRUPAL-6--1-0-0:
 * by louis : Changed mresource_resmems schema in mresource.install to reflect 'data' column as 'normal' size. Original schema had 'small' datatype and caused problems with long text during saves. 
 * by louis : added schema update hook to mresource.install to change the 'data' column to the TEXT datatype. 

